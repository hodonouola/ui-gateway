import React, { useContext, useEffect, useState ,useRef} from 'react'
import '../Account.css';
import '../../../../theme/Table.css'

import Header from '../../../Layouts/Header';
import Footer from '../../../Layouts/Footer';
import Auth from '../../../../contexts/Auth';
import {getUser} from '../../../../services/user.service';
import {getAllAppServices} from '../../../../services/userApplication.service'
import {logOut} from '../../../../services/Auth';
import { useHistory } from 'react-router-dom';
import { NavLink } from 'react-router-dom';
import copyIcon from '../../../../img/copy.png';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {infoNotif} from  '../../../../utils/notification'
import UserContext from '../../../../contexts/UserContext';


const DataTable = ({data}) =>{
  const columnnsheader = ["id","nom de l'application","url de l'application","Plateforme","Numéro de transaction","Token généré","Date de création"]


 const columns = data[0] && Object.keys(data[0])



  return (
    
                    <table class="table tablee table-striped table-bordered"  style={{width:'100%'}}>
                          <thead >
                              {/**<tr>
                                <th>Id</th>
                                <th>Nom de l'application</th>
                                <th>Url de l'application </th>
                                <th>Plateforme</th>
                                <th>Numéro de transaction</th>
                                <th>Token généré</th>
                                <th>Date de création</th>
  </tr>**/}
<tr>
                              {columnnsheader && columnnsheader.map((heading) => <th>{heading}</th>)}
                             </tr>
                             
                          </thead>
                          <tbody>

                            {data.map((row) => (
                              <tr>
                                {columns.map((column) => (
                                  <td>{row[column]}</td>
                                ))}
                              </tr>
                            ))}

                          
                         {/** {data.map((row) => (
                              <tr>
                                {columns.map((column) => (
                                  <td>{row[column]}</td>
                                ))}
                              </tr>
                            ))}*/} 

                          
                          </tbody>
                          <tfoot>
                              <tr>
                                <th>Id</th>
                                  <th>Nom de l'application</th>
                                  <th>Url de l'application </th>
                                  <th>Plateforme</th>
                                  <th>Numéro de transaction</th>
                                  <th>Token générer</th>
                                  <th>Date de création</th>
                              </tr>
                          </tfoot>
                  </table>             

                  
           

  )
}

export default DataTable;